class Start {
    constructor() {
        this._playerName = 'PLAYER 1';
        this._compName = 'COM';
        this._playerSelection;
        this._compSelection;
        this._winner;
    };

    compRandom() {
        const selection = ["🖐", "✌", "✊"];
        const compSelect = selection[Math.floor(Math.random() * selection.length)];

        return compSelect;
    };

    winCalculate() {
        if(this._playerSelection == '🖐' && this._compSelection == '✊') {
            return this._winner = this._playerName;
        } else if(this._playerSelection == '✊' && this._compSelection == '✌') {
            return this._winner = this._playerName;
        } else if(this._playerSelection == '✌' && this._compSelection == '🖐') {
            return this._winner = this._playerName;
        } else if(this._playerSelection == '🖐' && this._compSelection == '✌') {
            return this._winner = this._compName;
        } else if(this._playerSelection == '✌' && this._compSelection == '✊') {
            return this._winner = this._compName;
        } else if(this._playerSelection == '✊' && this._compSelection == '🖐') {
            return this._winner = this._compName;
        } else {
            return this._winner = 'DRAW';
        }
    };

    changeBackgroundComp() {
        const comp = document.getElementsByClassName('comp');

        if(this._compSelection == '✊') {
            comp[0].style.backgroundColor = '#C4C4C4';
        } else if (this._compSelection == '🖐') {
            comp[1].style.backgroundColor = '#C4C4C4';
        } else {
            comp[2].style.backgroundColor = '#C4C4C4';
        }
    };

    changeBackgroundPlayer() {
        const player = document.getElementsByClassName('player-1');

        if(this._playerSelection == '✊') {
            player[0].style.backgroundColor = '#C4C4C4';
        } else if(this._playerSelection == '🖐') {
            player[1].style.backgroundColor = '#C4C4C4';
        } else {
            player[2].style.backgroundColor = '#C4C4C4';
        }
    };

    matchResult() {
        if(this._winner != 'DRAW') {
            return `${this._winner} WIN`;
        } else {
            return `${this._winner}`;
        }
    };

    consoleResult() {
        const result = document.getElementById('result');
        const box = document.getElementsByClassName('box-result');
        const versus = document.getElementsByClassName('versus');
    
        versus[0].classList.add('d-none');
        box[0].classList.remove('d-none');
        result.textContent = this.matchResult();

        console.log(`kamu memilih, ${this._playerSelection} VS computer memilih, ${this._compSelection}`);
    
        if(this._winner == 'DRAW') {
            box[0].style.backgroundColor = '#035B0C';
            setTimeout(() => {
                console.log(`Hasilnya, ${this._winner}. Jadi tidak ada yang menang atau kalah nih.`);

                Swal.fire({
                    icon: 'info',
                    title: 'Oops...',
                    text: 'Hasilnya DRAW',
                    footer: '<p>Silakan klik tombol refresh<br>untuk memulai kembali.</p>'
                });
            }, 1500);
        } else if(this._winner == this._playerName) {
            console.log(`Pemenangnya adalah, ${this._playerSelection}`);
            setTimeout(() => {
                console.log(`Jadi yang memenangkan game adalah, ${this._winner}`);

                Swal.fire({
                    icon: 'success',
                    title: 'Selamat...',
                    text: 'Kamu adalah pemenangnya!!!',
                    footer: '<p>Silakan klik tombol refresh<br>untuk memulai kembali.</p>'
                });
            }, 1500);      
        } else {
            console.log(`Pemenangnya adalah, ${this._compSelection}`);
            setTimeout(() => {
                console.log(`Jadi yang memenangkan game adalah, ${this._winner}`);

                Swal.fire({
                    icon: 'error',
                    title: 'Yahh...',
                    text: 'Kamu kalahhh.. jangan nangis ya..',
                    footer: '<p>Silakan klik tombol refresh<br>untuk memulai kembali.</p>'
                });
            }, 1500);
        };
    };

    startPlay() {
        this.changeBackgroundPlayer();
        this.changeBackgroundComp();
        this.winCalculate();
        this.consoleResult();
    };
}

const startGame = (params) => {
    const start = new Start();
    start._playerSelection = params;
    start._compSelection = start.compRandom();
    start.startPlay();
}  

const refresh = () => {
    location.reload();
}
